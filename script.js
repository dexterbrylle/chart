(function () {
  'use strict';

  // client
  new Chartist.Bar('#d-client', {
    labels: ['Javascript', 'HTML', 'CSS', 'SASS', 'AngularJS', 'ReactJS', 'jQuery', 'JSON', 'XML'],
    series: [
      [4, 5, 5, 4, 4, 2, 5, 5, 5],
      [5, 5, 5, 3.5, 5, 1, 1, 5, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // server
  new Chartist.Bar('#d-server', {
    labels: ['NodeJS', 'ExpressJS', 'HapiJS', 'Java', 'Python', 'Flask', 'Django', 'PHP', 'Laravel', 'Java', 'C#'],
    series: [
      [4, 4, 2, 2, 2.5, 2, 1, 2.5, 2, 1.5, 2.5],
      [5, 5, 1.5, 1, 1, 1, 1, 0.5, 0.5, 1, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // db
  new Chartist.Bar('#d-db', {
    labels: ['MS SQL', 'MySQL', 'MongoDB', 'Redis', 'PostgreSQL'],
    series: [
      [3, 4, 4, 2.5, 2],
      [0.5, 3, 5, 1.5, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // applications
  new Chartist.Bar('#d-applications', {
    labels: ['Sublime Text', 'VS Code', 'Windows', 'macOS', 'Ubuntu'],
    series: [
      [5, 5, 5, 5, 4],
      [5, 5, 2.5, 5, 4]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // tools
  new Chartist.Bar('#d-tools', {
    labels: ['Git', 'SVN', 'Gulp', 'Grunt', 'Babel', 'Mocha', 'Chai', 'Webpack'],
    series: [
      [4, 3, 4, 4, 2.5, 1.5, 1.5, 1],
      [5, 0, 5, 2, 1, 1, 1, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // servers

  new Chartist.Bar('#d-systems', {
    labels: ['Nginx', 'Apache', 'AWS EC2', 'AWS S3', 'DigitalOcean', 'Google Cloud Platform', 'Firebase', 'Heroku', 'Azure'],
    series: [
      [3, 3, 4, 3, 4, 3, 3, 3, 3],
      [5, 2, 3, 2, 5, 1.5, 1.5, 2, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });
})();

(function () {
  'use strict';

  // client
  new Chartist.Bar('#r-client', {
    labels: ['Javascript', 'HTML', 'CSS', 'AngularJS'],
    series: [
      [3, 2, 2, 2],
      [3, 2, 2, 2]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // server
  new Chartist.Bar('#r-server', {
    labels: ['NodeJS', 'ExpressJS', 'Java'],
    series: [
      [2, 2, 2],
      [2, 2, 2]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // db
  new Chartist.Bar('#r-db', {
    labels: ['MS SQL', 'MySQL', 'MongoDB', 'PostgreSQL'],
    series: [
      [3, 3, 1, 2],
      [3, 3, 1, 2]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // applications
  new Chartist.Bar('#r-applications', {
    labels: ['VS Code', 'Windows', 'macOS', 'Ubuntu'],
    series: [
      [5, 5, 2, 1],
      [5, 5, 2, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // tools
  new Chartist.Bar('#r-tools', {
    labels: ['Git', 'Grunt', 'Maven', 'Apache'],
    series: [
      [5, 4, 4, 3],
      [5, 4, 4, 3]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // testing

  new Chartist.Bar('#r-testing', {
    labels: ['Manual/Functional', 'Automation Testing', 'Performance Testing', 'API Testing', 'Selenium (Cucumber)', 'ALM/QC', 'UFT'],
    series: [
      [5, 4, 3, 3, 4, 4, 4],
      [5, 5, 2, 2, 5, 5, 2]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });
})();

(function () {
  'use strict';

  // client
  new Chartist.Bar('#z-client', {
    labels: ['HTML', 'CSS3', 'SASS', 'LESS', 'Bootstrap', 'Javascript', 'jQuery', 'Coffeescript', 'JSX', 'Handlebars', 'AngularJS', 'React Native', 'Zepto', 'Ember.js', 'Require.js', 'JSP', 'JSF', 'Thymeleaf'],
    series: [
      [4, 4, 2, 2, 3, 5, 5, 3, 3, 4, 4, 2, 3, 3, 2, 4, 4, 4],
      [4, 4, 2, 2, 3, 4, 4, 2, 3, 3, 3, 2, 2, 2, 1, 4, 4, 4]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // server
  new Chartist.Bar('#z-server', {
    labels: ['NodeJS', 'NPM', 'Express.JS', 'Webpack', 'Babel', 'Java 6', 'Java 7', 'Java 8', 'Java Concurrency', 'JMS'],
    series: [
      [3, 3, 2, 2, 1, 5, 5, 4, 4, 3],
      [4, 4, 2, 2, 1, 4, 5, 3, 3, 2]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 500,
    onlyInteger: true,
    referenceValue: 5
  });

  new Chartist.Bar('#z-server2', {
    labels: ['Bean Validation', 'JPA', 'JTA', 'Java Security Constraint', 'JPQL', 'JAVA EL', 'JAX-RS (REST)', 'JAX-RS (SOAP)', 'Jersey Rest', 'JNDI'],
    series: [
      [4, 4, 4, 3, 4, 4, 3, 4, 4, 4, 4],
      [4, 5, 4, 1, 2, 4, 2, 4, 3, 4, 3]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 500,
    onlyInteger: true,
    referenceValue: 5
  });

  new Chartist.Bar('#z-server3', {
    labels: ['Spring Core', 'Spring Web', 'Spring Webflow', 'Spring MVC', 'Spring Security', 'Spring AOP', 'Spring Data', 'Spring Integration', 'Spring Batch', 'Spring Boot', 'Spring oAuth/oAuth2', 'Spring Test', 'Hibernate', 'Liquibase'],
    series: [
      [4, 4, 3, 5, 5, 3, 1, 3, 3, 4, 3, 1, 5, 3],
      [5, 4, 3, 4, 5, 2, 2, 3, 3, 5, 4, 1, 4, 3]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 500,
    onlyInteger: true,
    referenceValue: 5
  });

  new Chartist.Bar('#z-server4', {
    labels: ['Checkstyle', 'Dozer', 'Jasper Reports', 'Dynamic Jasper', 'OpenCMIS', 'SAP HANA API', 'SuccessFactor API', 'GSON', 'JUnit 4', 'Mockito/PowerMock', 'Wiremock', 'Android Development', 'Jmeter'],
    series: [
      [3, 3, 5, 5, 3, 3, 3, 0, 4, 5, 3, 3, 3],
      [3, 3, 4, 3, 3, 2, 5, 0, 5, 5, 2, 3, 2]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 500,
    onlyInteger: true,
    referenceValue: 5
  });

  // db
  new Chartist.Bar('#z-db', {
    labels: ['Oracle', 'MySQL', 'MariaDB', 'Postgres', 'H2', 'MS SQL', 'MongoDB/NoSQL'],
    series: [
      [5, 5, 3, 4, 4, 3, 4],
      [4, 5, 2, 4, 4, 1, 4]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // applications
  new Chartist.Bar('#z-applications', {
    labels: ['IntelliJ IDEA', 'Eclipse', 'iReport', 'Atom', 'DBeaver'],
    series: [
      [5, 3, 4, 3, 5],
      [5, 2, 3, 3, 5]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // tools
  new Chartist.Bar('#z-tools', {
    labels: ['Git', 'Mercurial', 'SVN', 'JIRA', 'Redmine', 'Kanban', 'RTC', 'Linux Ubuntu', 'Fedora', 'Mac OSX', 'Microsoft Windows'],
    series: [
      [5, 5, 5, 4, 3, 3, 3, 3, 4, 3, 3],
      [5, 2, 2, 5, 3, 3, 3, 4, 5, 4, 4]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  new Chartist.Bar('#z-build', {
    labels: ['Grunt', 'Gulf', 'Maven', 'Ivy', 'Ant', 'Hudson', 'Jenkins', 'Shippable', 'SonarQube'],
    series: [
      [3, 3, 5, 2, 3, 1, 4, 1, 4],
      [3, 2, 4, 2, 3, 1, 5, 2, 3]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // testing

  new Chartist.Bar('#z-systems', {
    labels: ['Tomcat 7', 'IBM Websphere', 'IBM Bluemix', 'Glassfish', 'SAP Hana Cloud', 'Web Logic'],
    series: [
      [5, 4, 3, 3, 4, 4],
      [5, 5, 2, 2, 5, 5]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });
})();
