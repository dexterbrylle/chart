(function () {
  'use strict';

  // client
  new Chartist.Bar('#d-client', {
    labels: ['Javascript', 'HTML', 'CSS', 'SASS', 'AngularJS', 'ReactJS', 'jQuery', 'JSON', 'XML'],
    series: [
      [4, 5, 5, 4, 4, 2, 5, 5, 5] ,
      [5, 5, 5, 4, 4, 1, 1, 5, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // server
  new Chartist.Bar('#d-server', {
    labels: ['NodeJS', 'ExpressJS', 'HapiJS', 'Java', 'Python', 'Flask', 'Django', 'PHP', 'Laravel', 'Java', 'C#'],
    series: [
      [4, 4, 2, 2, 2.5, 2, 1, 2.5, 2, 1.5, 2.5],
      [5, 5, 2, 1, 2, 2, 1, 1, 1, 1, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // db
  new Chartist.Bar('#d-db', {
    labels: ['MS SQL', 'MySQL', 'MongoDB', 'Redis', 'PostgreSQL'],
    series: [
      [3, 4, 4, 2.5, 2],
      [1, 2.5, 5, 2, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // applications
  new Chartist.Bar('#d-applications', {
    labels: ['Sublime Text', 'VS Code', 'Windows', 'macOS', 'Ubuntu'],
    series: [
      [5, 5, 5, 5, 4],
      [5, 5, 2.5, 5, 4]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // tools
  new Chartist.Bar('#d-tools', {
    labels: ['Git', 'Gulp', 'Grunt', 'Webpack'],
    series: [
      [4, 4, 4, 1],
      [5, 5, 2, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });

  // servers

  new Chartist.Bar('#d-systems', {
    labels: ['Nginx', 'Apache', 'AWS EC2', 'AWS S3', 'DigitalOcean', 'Google Cloud Platform', 'Firebase', 'Heroku', 'Azure'],
    series: [
      [3, 3, 4, 3, 4, 3, 3, 3, 3],
      [5, 2, 3, 2, 5, 1.5, 1.5, 2, 1]
    ]
  }, {
    seriesBarDistance: 5,
    reverseData: true,
    horizontalBars: true,
    axisY: {
      offset: 150
    },
    axisX: {
      offset: 200,
      labelInterpolation: (value) => {
        return 'USAGE' + value;
      }
    },
    high: 5,
    low: 0,
    width: 500,
    height: 450,
    onlyInteger: true,
    referenceValue: 5
  });
})();
